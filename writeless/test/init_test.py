# -*- coding: utf-8 -*-
"""
Created on Mon Feb 16 10:27:11 2015

@author: thomas
"""

import logging
import StringIO
import unittest

import writeless
import writeless.test.data as data
import writeless.wio


class TestLogging(unittest.TestCase):
    """
    Tests for open and openUTF
    """

    def setUp(self):
        pass

    def test_open_(self):
        """
        Test logging mechanism.
        """

        string_stream = StringIO.StringIO()

        logging.basicConfig(stream=string_stream, level=0)
        file_handle = writeless.wio.wopen(data.TEXT_TXT)
        file_handle.close()

        actual = string_stream.getvalue()
        expected = ('INFO:writeless.wio:wopen: r %s text/plain;'
                    + ' charset=utf-8\n') % data.TEXT_TXT
        self.assertEquals(expected, actual)

        string_stream.truncate(0)

        file_handle = writeless.wio.wopen(data.TEXT_TXT_BZ2)
        file_handle.close()

        actual = string_stream.getvalue()
        expected = ('INFO:writeless.wio:wopen: r %s application/x-bzip2;'
                    + ' charset=binary\n') % data.TEXT_TXT_BZ2
        self.assertEquals(expected, actual)


def main():
    """ Execute tests. """
    unittest.main(exit=False)

if __name__ == '__main__':
    main()
