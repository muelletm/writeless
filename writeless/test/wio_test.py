# -*- coding: utf-8 -*-
"""
Tests for the io methods.
"""

import random
import string
import unittest

import writeless.test.data as data
import writeless.wio


class TestOpenFunctions(unittest.TestCase):
    """
    Tests for open and openUTF
    """

    def setUp(self):
        pass

    def test_open_(self):
        """
        Test open() by reading compressed and uncompressed files.
        """

        text = open(data.TEXT_TXT).read()

        self.assertEquals(text, writeless.wio.wopen(data.TEXT_TXT).read())
        self.assertEquals(text, writeless.wio.wopen(data.TEXT_TXT_BZ2).read())
        self.assertEquals(text, writeless.wio.wopen(data.TEXT_TXT_GZ).read())

    def test_open_utf8_(self):
        """
        Test open_utf8() by reading methods with different encodings.
        """

        expected_text = unicode(open(data.TEXT_UTF8_TXT).read(), 'utf-8')
        actual_text = writeless.wio.wopen_utf8(data.TEXT_UTF8_TXT).read()
        self.assertEquals(expected_text, actual_text)

        expected_text = unicode(open(data.TEXT_LATIN1_TXT).read(), 'latin1')
        actual_text = writeless.wio.wopen_utf8(data.TEXT_LATIN1_TXT,
                                               encoding='latin1').read()
        self.assertEquals(expected_text, actual_text)


class TestAsciiEncoder(unittest.TestCase):
    """
    Tests the AsciiEncoder
    """

    def setUp(self):
        pass

    def roundtrip_(self, i, exclude=None):
        """
        Helper. Tests whether encode followed by decode returns
        original input.
        """
        encoder = writeless.wio.AsciiEncoder(exclude)
        code = encoder.encode(i)

        if exclude:
            for char in exclude:
                self.assertNotIn(char, code)

        actual_i = encoder.decode(code)
        self.assertEquals(i, actual_i)

    def test_encode_decode_(self):
        """
        Run randomized roundtrips. Test exclude by excluding ASCII letters.
        """

        rng = random.Random(42)
        for _ in range(1000):
            rand = rng.randint(100000, 100000000)
            self.roundtrip_(rand, exclude=string.ascii_letters)
        self.roundtrip_(1000, exclude='#')
        self.roundtrip_(1000000, exclude='#')


def main():
    """ Execute tests. """
    unittest.main(exit=False)

if __name__ == '__main__':
    main()
