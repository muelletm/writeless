# -*- coding: utf-8 -*-
"""
 A list of resource file paths.
"""

import os

import writeless.test

DATA = os.path.join(os.path.split(writeless.test.__file__)[0], 'data')
TEXT_TXT = os.path.join(DATA, "text.txt")
TEXT_TXT_BZ2 = os.path.join(DATA, "text.txt.bz2")
TEXT_TXT_GZ = os.path.join(DATA, "text.txt.gz")
TEXT_UTF8_TXT = os.path.join(DATA, "text.utf8.txt")
TEXT_LATIN1_TXT = os.path.join(DATA, "text.latin1.txt")
