# -*- coding: utf-8 -*-
"""
Created on Sun Feb 15 09:56:50 2015

Writeless input/output methods.

@author: Thomas Müller
"""

import bz2
import codecs
import gzip
import logging
import string

try:
    import magic
    MAGIC = magic.open(magic.MIME)
    MAGIC.load()
except ImportError:
    MAGIC = None

try:
    import lzma
    LZMA = lzma
except ImportError:
    LZMA = None


def wopen(filename, mode='r', buffering=1, compresslevel=9):
    """
    Open a file depending on its extension using gzip, bz2 or the open builtin.
    If lzma is installed .xy and .lzma are also supported.
    """

    msg = 'wopen: %s %s' % (mode, filename)
    if 'r' in mode and MAGIC:
        fingerprint = MAGIC.file(filename)
        if fingerprint:
            msg = 'wopen: %s %s %s' % (mode, filename, fingerprint)
    logging.getLogger(__name__).info(msg)

    if filename.endswith('.gz'):
        return gzip.open(filename, mode, compresslevel)

    if filename.endswith('.bz2'):
        return bz2.BZ2File(filename, mode, buffering, compresslevel)

    if filename.endswith('.xy') or filename.endswith('.lzma'):
        if LZMA:
            return LZMA.LZMAFile(filename, mode, buffering)
        else:
            logging.getLogger(__name__).warn('Not using lzma, couldn\'t '
                                             + 'load module.')

    return open(filename, mode, buffering)


def wopen_utf8(filename, mode='r', encoding='utf-8', errors='strict',
               buffering=1):
    """
    Open a file using codecs.StreamReaderWriter.
    """

    if encoding is not None:
        if 'U' in mode:
            # No automatic conversion of '\n' is done on reading and writing
            mode = mode.strip().replace('U', '')
            if mode[:1] not in set('rwa'):
                mode = 'r' + mode
        if 'b' not in mode:
            # Force opening of the file in binary mode
            mode = mode + 'b'

    file_handle = wopen(filename, mode, buffering)
    if encoding is None:
        return file_handle
    info = codecs.lookup(encoding)
    srw = codecs.StreamReaderWriter(file_handle,
                                    info.streamreader,
                                    info.streamwriter, errors)
    # Add attributes to simplify introspection
    srw.encoding = encoding
    return srw


class AsciiEncoder(object):
    """
    Encodes an positive integer into a string with high base. The output
    alphabet consists of all printable nonspace ASCII characters.

    A list/string of ASCII characters to exclude can be provided.
    """

    def __init__(self, exclude=None):
        self.alphabet_ = []

        if exclude:
            exclude = set(exclude)
        else:
            exclude = []

        for char in string.printable:
            if not (char.isspace() or char in exclude):
                self.alphabet_.append(char)

        self.alphabet_map_ = {}
        for index, char in enumerate(self.alphabet_):
            self.alphabet_map_[char] = index

        assert len(self.alphabet_map_) == len(self.alphabet_)

    def encode(self, positive):
        """
        Encode positive integer into string.
        """
        assert positive >= 0

        num_symbols = len(self.alphabet_)

        output = []
        while True:
            digit = positive % num_symbols
            assert digit >= 0 and digit < num_symbols
            positive = positive / num_symbols
            assert positive >= 0
            output.append(self.alphabet_[digit])
            if positive == 0:
                break

        output.reverse()
        return ''.join(output)

    def decode(self, code):
        """
        Decode code word (string) into positive.
        """

        assert code

        num_symbols = len(self.alphabet_)

        output = 0
        for char in code:
            output *= num_symbols
            output += self.alphabet_map_[char]

        return output
